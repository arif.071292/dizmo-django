from django.db import models
from profiles.models import Profile
from category.models import Category, SubCategory


class Job(models.Model):
    title       = models.CharField(max_length=500)

    # Creator and Developer
    employer	= models.ForeignKey(Profile, related_name="employer", on_delete=models.CASCADE)
    employee	= models.ForeignKey(Profile, related_name="employee", on_delete=models.CASCADE, null=True)

    category    = models.ForeignKey(Category, on_delete=models.CASCADE)
    subcategory	= models.ForeignKey(SubCategory, on_delete=models.CASCADE)
    description = models.CharField(max_length=500,default="")
    deadline    = models.DateTimeField(null=True)
    completion  = models.DateTimeField(null=True)
    isFeatured  = models.BooleanField(default=False)
    created     = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated     = models.DateTimeField(auto_now_add=False, auto_now=True)

    # Feedback
    # Employer
    employerRating = models.FloatField(null=True)
    employerReview = models.TextField(blank=True, null=True)
    
    # Employee
    employeeRating = models.FloatField(null=True)
    employeeReview = models.TextField(blank=True, null=True)

    class Meta:
        ordering        = ['isFeatured','-created']
        unique_together = ('title','employer')


class Application(models.Model):
    user	= models.ForeignKey(Profile, on_delete=models.CASCADE)
    job    	= models.ForeignKey(Job, on_delete=models.CASCADE)
    created	= models.DateTimeField(auto_now_add=True, auto_now=False)

    class Meta:
        ordering        = ['user','job']
        unique_together = ("user", "job")