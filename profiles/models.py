import uuid
from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model): 
    hid         = models.UUIDField(default=uuid.uuid4) # Hidden ID
    email       = models.EmailField(unique=True, db_index=True)
    password    = models.CharField(max_length=120)
    role        = models.CharField(max_length=15)
    name        = models.CharField(max_length=15,blank=True,null=True)
    website     = models.CharField(max_length=200,blank=True,null=True)
    description = models.CharField(max_length=1200,blank=True,null=True)
    coverphoto  = models.FileField(upload_to='CoverPhoto/%Y/%m/%d',null=True,blank=True)
    propic      = models.FileField(upload_to='ProfilePhoto/%Y/%m/%d',null=True,blank=True)
    isVerified  = models.BooleanField(default=False)
    created     = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated     = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        ordering = ('-created',)