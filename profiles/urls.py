from django.conf.urls import url
from profiles import views

urlpatterns = [
	url(r'^login/$', views.login),
    url(r'^user/$', views.create_user),
    url(r'^user/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.user_list),
    url(r'^user/(?P<id>[0-9]+)/$', views.user_detail),
    url(r'^user/change/email/(?P<id>[0-9]+)/$', views.change_email),
    url(r'^user/change/password/(?P<id>[0-9]+)/$', views.change_password),
]