from django.conf.urls import url, include
from rest_framework import routers

router = routers.DefaultRouter()

urlpatterns = [
	url(r'^', include(router.urls)),
	url(r'^', include('job.urls')),
	url(r'^', include('chat.urls')),
    url(r'^', include('profiles.urls')),
    url(r'^', include('category.urls')),
    url(r'^', include('staticpage.urls')),
    url(r'^', include('notification.urls')),
    url(r'^docs/', include('rest_framework_swagger.urls')),
    url(r'^drf/docs/', include('rest_framework_docs.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]