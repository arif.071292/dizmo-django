from models import Category, SubCategory, Subscription
from rest_framework import serializers


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', 'created', 'updated')


class SubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model 	= SubCategory
        fields 	= ('id', 'name', 'category', 'created', 'updated')


class UpdateSubCategorySerializer(serializers.Serializer):
    name 		= serializers.CharField(required=False)
    category 	= serializers.IntegerField(required=False)


class SubscriptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subscription
        fields = ('id', 'user', 'category', 'created')


