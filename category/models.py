from django.db import models
from profiles.models import Profile


class Category(models.Model):
    name        = models.CharField(max_length=30, unique=True)
    created     = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated     = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        ordering = ['name']


class SubCategory(models.Model):
    name        = models.CharField(max_length=30)
    category    = models.ForeignKey(Category, on_delete=models.CASCADE)
    created     = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated     = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        ordering        = ['category','name']
        unique_together = ("name", "category")


class Subscription(models.Model):
    user        = models.ForeignKey(Profile, on_delete=models.CASCADE)
    category    = models.ForeignKey(Category, on_delete=models.CASCADE)
    created     = models.DateTimeField(auto_now_add=True, auto_now=False)

    class Meta:
        ordering        = ['user','category']
        unique_together = ("user", "category")