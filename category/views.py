from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from profiles.models import Profile
from models import Category, SubCategory, Subscription
from serializers import CategorySerializer, SubCategorySerializer, SubscriptionSerializer, UpdateSubCategorySerializer


###### Category ######

@api_view(['GET', 'POST'])
def category_list(request):
    """
    List all categories, or create a new category.
    ---
    request_serializer: CategorySerializer
    """
    if request.method == 'GET':
        categories = Category.objects.all()
        serializer = CategorySerializer(categories, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = CategorySerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET', 'PUT', 'DELETE'])
def category_detail(request, id):
    """
    retrieve, update or delete a category instance.
    ---
    request_serializer: CategorySerializer
    """

    try:
        category = Category.objects.get(pk=id)
    except Category.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = CategorySerializer(category)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = CategorySerializer(category, partial=True, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        category.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



###### SubCategory ######

@api_view(['GET', 'POST'])
def subcategory_list(request):
    """
    List all subcategories, or create a new subcategory.
    ---
    request_serializer: SubCategorySerializer
    """

    if request.method == 'GET':
        subcategories = SubCategory.objects.all()
        serializer = SubCategorySerializer(subcategories, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = SubCategorySerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def subcategory_detail(request, id):
    """
    retrieve, update or delete a subcategory instance.
    ---
    request_serializer : UpdateSubCategorySerializer
    """

    try:
        subcategory = SubCategory.objects.get(pk=id)
    except SubCategory.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = SubCategorySerializer(subcategory)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = SubCategorySerializer(subcategory, partial=True, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        subcategory.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



###### Subscription ######

@api_view(['POST'])
def create_subscription(request):
    """
    create a new subscription.
    ---
    request_serializer: SubscriptionSerializer
    """

    if request.method == 'POST':
        serializer = SubscriptionSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def user_subscription_list(request, id, pageName, pageSize):
    """
    list all subscriptions of given user
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        subscriptions = Subscription.objects.filter(user=id)[start:end]
        serializer = SubscriptionSerializer(subscriptions, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def category_subscription_list(request, id, pageName, pageSize):
    """
    list all subscriptions of given category
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        subscriptions = Subscription.objects.filter(category=id)[start:end]
        serializer = SubscriptionSerializer(subscriptions, many=True)
        return Response(serializer.data)


@api_view(['GET', 'DELETE'])
def subscription_detail(request, id):
    """
    retrieve or delete a subscription instance.
    """

    try:
        subscription = Subscription.objects.get(pk=id)
    except Subscription.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = SubscriptionSerializer(subscription)
        return Response(serializer.data)

    elif request.method == 'DELETE':
        subscription.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)