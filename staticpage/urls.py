from django.conf.urls import url
from staticpage import views

urlpatterns = [
    url(r'^faq/$', views.faq_list),
    url(r'^faq/(?P<id>[0-9]+)/$', views.faq_detail),
    url(r'^staticpage/$', views.staticpage_list),
    url(r'^staticpage/(?P<id>[0-9]+)/$', views.staticpage_detail),
]