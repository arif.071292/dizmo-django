from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from models import Notification
from django.db.models import Q
from serializers import NotificationSerializer


@api_view(['POST'])
def create_notification(request):
    """
    create a new notification.
    ---
    request_serializer: NotificationSerializer
    """

    if request.method == 'POST':
        serializer = NotificationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            print serializer.data
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def user_notification_list(request, id, pageName, pageSize):
    """
    list all notification for given user
    """

    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        notifications = Notification.objects.filter(user=id)[start:end]
        serializer = NotificationSerializer(notifications, many=True)
        return Response(serializer.data)


@api_view(['DELETE'])
def notification_detail(request, id):
    """
    delete a notification instance.
    """

    try:
        notification = Notification.objects.get(pk=id)
    except Notification.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'DELETE':
        notification.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)