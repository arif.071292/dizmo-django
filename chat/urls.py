from django.conf.urls import url
from chat import views

urlpatterns = [
	url(r'^message/$', views.create_chat),
    url(r'^message/user/(?P<id>\d+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.user_chat_list),
    url(r'^message/users/(?P<id1>\d+)/(?P<id2>\d+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.two_user_chat_list),
    url(r'^message/(?P<id>[0-9]+)/$', views.chat_detail),
]