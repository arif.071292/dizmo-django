from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from models import Chat
from django.db.models import Q
from serializers import ChatSerializer


@api_view(['GET','POST'])
def create_chat(request):
    """
    create a new message.
    ---
    request_serializer: ChatSerializer
    """
    
    if request.method == 'POST':
        serializer = ChatSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            print serializer.data
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def user_chat_list(request, id, pageName, pageSize):
    """
    list all message for given user
    """

    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        chats = Chat.objects.filter(Q(sender=id) | Q(receiver=id))[start:end]
        serializer = ChatSerializer(chats, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def two_user_chat_list(request, id1, id2, pageName, pageSize):
    """
    list all message between two user
    """

    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        chats = Chat.objects.filter(Q(sender=id1,receiver=id2) | Q(sender=id2,receiver=id1))[start:end]
        serializer = ChatSerializer(chats, many=True)
        return Response(serializer.data)


@api_view(['DELETE'])
def chat_detail(request, id):
    """
    delete a message instance.
    """

    try:
        chat = Chat.objects.get(pk=id)
    except Chat.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'DELETE':
        chat.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)